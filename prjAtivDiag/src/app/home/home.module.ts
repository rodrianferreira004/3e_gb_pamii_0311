import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';
import { CardImovelComponent } from './card-imovel/card-imovel.component';

import { HomePageRoutingModule } from './home-routing.module';
import { PromocaoComponent } from './promocao/promocao.component';
import { AlugarComponent } from './alugar/alugar.component';
import { ComprarComponent } from './comprar/comprar.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule
  ],
  declarations: [HomePage, CardImovelComponent, PromocaoComponent, AlugarComponent, ComprarComponent]
})
export class HomePageModule {}
