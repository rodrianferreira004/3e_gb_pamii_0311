import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-card-imovel',
  templateUrl: './card-imovel.component.html',
  styleUrls: ['./card-imovel.component.scss'],
})


export class CardImovelComponent implements OnInit {


  slideOpts = {
    slidesPerView: 2,
    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: true,
    }
  }
  constructor() { }

  ngOnInit() {}

}
