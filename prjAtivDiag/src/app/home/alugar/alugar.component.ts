import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-alugar',
  templateUrl: './alugar.component.html',
  styleUrls: ['./alugar.component.scss'],
})
export class AlugarComponent implements OnInit {
  slideOpts = {
    slidesPerView: 2,
    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: true,
    }
  }
  constructor() { }

  ngOnInit() {}

}
